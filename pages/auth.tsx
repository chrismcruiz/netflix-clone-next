import Form from "@/components/Form";
import Header from "@/components/Header";

const Auth = () => {
  return (
    <div className="relative h-full bg-[url('/images/hero.jpg')] bg-no-repeat bg-center bg-fixed bg-cover">
      <div className="bg-black h-full lg:bg-opacity-50">
        <Header />
        <Form />
      </div>
    </div>
  );
};

export default Auth;
