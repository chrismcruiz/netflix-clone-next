import { useCallback, useState } from "react";

import Input from "@/components/Input";
import Variant from "@/constants/variants";

const Form = () => {
  const [email, setEmail] = useState("");
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const [variant, setVariant] = useState(Variant.Login);

  const toggleVariant = useCallback(
    () =>
      setVariant((currentVariant) =>
        currentVariant === Variant.Login ? Variant.Register : Variant.Login
      ),
    []
  );

  return (
    <div className="flex justify-center">
      <div className="bg-black/70 p-12 2xl:p-16 w-full lg:w-2/5 lg:max-w-md rounded-md mt-2">
        <h2 className="text-white text-4xl mb-4 2xl:mb-8 font-semibold">
          {variant === Variant.Login ? "Sign in" : "Register"}
        </h2>
        <div className="flex flex-col gap-4">
          {variant === Variant.Register && (
            <Input
              label="Username"
              updateValue={setUser}
              id="user"
              value={user}
            />
          )}
          <Input
            label="Email"
            updateValue={setEmail}
            id="email"
            type="email"
            value={email}
          />
          <Input
            label="Password"
            updateValue={setPassword}
            id="password"
            type="password"
            value={password}
          />
          <button
            type="button"
            className="bg-red-600 py-3 text-white rounded-md mt-8 2xl:mt-10 hover:bg-red-700 transition"
          >
            {variant === Variant.Login ? "Login" : "Sign up"}
          </button>
          <p className="text-neutral-500 mt-8 2xl:mt-12">
            {variant === Variant.Login
              ? "First time using Netflix?"
              : "Already have an account?"}
            <span
              onClick={toggleVariant}
              className="text-white ml-1 hover:underline cursor-pointer"
            >
              {variant === Variant.Login ? "Create an account" : "Login"}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Form;
