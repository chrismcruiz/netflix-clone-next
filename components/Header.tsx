import Image from "next/image";

const Header = () => {
  return (
    <nav className="px-12 py-5">
      <Image
        className="h-12"
        src="/images/logo.png"
        alt="Logo"
        height={100}
        width={180}
      />
    </nav>
  );
};

export default Header;
